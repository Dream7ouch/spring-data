package com.example.springdata.util.paginator;

import org.springframework.data.domain.Page;

import java.util.ArrayList;
import java.util.List;

public class PageRender<T> {

    private String url;
    private Page<T> page;

    private int totalPages;

    private int actualPage;

    private List<PageItem> paginas;

    public PageRender(String url, Page<T> page) {
        this.url = url;
        this.page = page;
        this.paginas = new ArrayList<>();

        int numberPerElements = page.getSize();
        totalPages = page.getTotalPages();
        actualPage = page.getNumber() + 1;

        int desde, hasta;
        if (totalPages <= numberPerElements) {
            desde = 1;
            hasta = totalPages;
        } else {
            if (actualPage <= numberPerElements / 2) {
                desde = 1;
                hasta = numberPerElements;
            } else if (actualPage >= totalPages - numberPerElements / 2) {
                desde = totalPages - numberPerElements + 1;
                hasta = numberPerElements;
            } else {
                desde = actualPage - numberPerElements / 2;
                hasta = numberPerElements;
            }
        }

        for (int i = 0; i < hasta; i++) {
            paginas.add(new PageItem(desde + i, actualPage == desde + i));
        }
    }

    public String getUrl() {
        return url;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public int getActualPage() {
        return actualPage;
    }

    public List<PageItem> getPaginas() {
        return paginas;
    }

    public boolean isFirst(){
        return page.isFirst();
    }

    public boolean isLast(){
        return  page.isLast();
    }

    public boolean hasNext(){
        return page.hasNext();
    }

    public boolean hasPrevious(){
        return page.hasPrevious();
    }
}
