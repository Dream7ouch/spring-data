package com.example.springdata;

import com.example.springdata.models.service.IUploadFileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringDataApplication implements CommandLineRunner {

    private final
    IUploadFileService uploadFileService;

    @Autowired
    public SpringDataApplication(@Qualifier("uploadFileServiceImpl") IUploadFileService uploadFileService) {
        this.uploadFileService = uploadFileService;
    }

    public static void main(String[] args) {
        SpringApplication.run(SpringDataApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        // uploadFileService.deleteAll();
        uploadFileService.init();
    }
}
