package com.example.springdata;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.nio.file.Paths;

@Configuration
public class MvcConfig implements WebMvcConfigurer {

    private Logger log = LoggerFactory.getLogger(getClass());

    /*@Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        String resorcePath = Paths.get("uploads").toAbsolutePath().toUri().toString();

        log.info("resorcePath: " + resorcePath);

        registry.addResourceHandler("/uploads/**")
                .addResourceLocations(resorcePath);
    }*/
}
