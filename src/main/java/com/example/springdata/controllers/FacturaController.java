package com.example.springdata.controllers;

import com.example.springdata.models.entity.Cliente;
import com.example.springdata.models.entity.Factura;
import com.example.springdata.models.service.IClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Map;
import java.util.Optional;

@Controller
@RequestMapping("/factura")
@SessionAttributes("factura")
public class FacturaController {

    private final IClienteService clienteService;

    @Autowired
    public FacturaController(IClienteService clienteService) {
        this.clienteService = clienteService;
    }

    @GetMapping("/form/{clienteId}")
    public String crear(@PathVariable(value = "clienteId") Long clienteId, Map<String, Object> model, RedirectAttributes flash) {
        Optional<Cliente> clienteOptional;
        Cliente cliente;
        if (clienteId > 0) {
            clienteOptional = clienteService.findOne(clienteId);
            if (clienteOptional.isPresent()) cliente = clienteOptional.get();
            else {
                flash.addFlashAttribute("error", "El cliente no existe en la base de datos");
                return "redirect:/listar";
            }
        } else {
            flash.addFlashAttribute("error", "El ID del cliente no puede ser cero");
            return "redirect:/listar";
        }

        Factura factura = new Factura();
        factura.setCliente(cliente);

        model.put("factura", factura);
        model.put("titulo", "Crear factura");
        return "factura/form";
    }

}
