package com.example.springdata.controllers;

import com.example.springdata.models.entity.Cliente;
import com.example.springdata.models.service.IClienteService;
import com.example.springdata.models.service.IUploadFileService;
import com.example.springdata.util.paginator.PageRender;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.sql.DataSource;
import javax.validation.Valid;
import java.io.IOException;
import java.net.MalformedURLException;
import java.sql.SQLException;
import java.util.Map;
import java.util.Optional;

@Controller
@SessionAttributes("cliente")
public class ClienteController {

    private final IClienteService clienteService;
    private final IUploadFileService uploadFileService;

    @Autowired
    public ClienteController(@Qualifier("clienteServiceImpl") IClienteService clienteService, @Qualifier("uploadFileServiceImpl") IUploadFileService uploadFileService) {
        this.clienteService = clienteService;
        this.uploadFileService = uploadFileService;
    }

    @RequestMapping(value = "/listar", method = RequestMethod.GET)
    public String listar(@RequestParam(name = "page", defaultValue = "0") int page, Model model) {
        Pageable pageRequest = PageRequest.of(page, 5);
        Page<Cliente> clientes = clienteService.findAll(pageRequest);
        PageRender<Cliente> pageRender = new PageRender<>("/listar", clientes);
        model.addAttribute("titulo", "Listado de clientes");
        model.addAttribute("clientes", clientes);
        model.addAttribute("page", pageRender);
        return "listar";
    }

    @RequestMapping(value = "ver/{id}")
    public String ver(@PathVariable(value = "id") Long id, Map<String, Object> model, RedirectAttributes flash) {
        Optional<Cliente> clienteOptional;
        Cliente cliente;
        if (id > 0) {
            clienteOptional = clienteService.findOne(id);
            if (clienteOptional.isPresent()) cliente = clienteOptional.get();
            else {
                flash.addFlashAttribute("error", "El ID del cliente no existe");
                return "redirect:/listar";
            }
        } else {
            flash.addFlashAttribute("error", "El ID del cliente no puede ser cero");
            return "redirect:/listar";
        }
        model.put("cliente", cliente);
        model.put("titulo", "Detalle del cliente: " + cliente.getName());
        return "ver";
    }

    @RequestMapping(value = "/form", method = RequestMethod.GET)
    public String crear(Map<String, Object> model) {
        Cliente cliente = new Cliente();
        model.put("cliente", cliente);
        model.put("titulo", "Formulario de cliente");
        return "form";
    }

    @RequestMapping(value = "/form/{id}", method = RequestMethod.GET)
    public String editar(@PathVariable(value = "id") Long id, RedirectAttributes flash, Map<String, Object> model) {
        Optional<Cliente> clienteOptional;
        Cliente cliente;
        if (id > 0) {
            clienteOptional = clienteService.findOne(id);
            if (clienteOptional.isPresent()) cliente = clienteOptional.get();
            else {
                flash.addFlashAttribute("error", "El ID del cliente no existe");
                return "redirect:/listar";
            }
        } else {
            flash.addFlashAttribute("error", "El ID del cliente no puede ser cero");
            return "redirect:/listar";
        }

        model.put("cliente", cliente);
        model.put("titulo", "Formulario de cliente");

        return "form";
    }

    @GetMapping(value = "/report/{reportName}")
    public ResponseEntity<Resource> verReporte(@PathVariable String reportName) {
        Resource resource = null;
        try {
            resource = uploadFileService.loadPdf(reportName, null);
        } catch (MalformedURLException | SQLException | JRException e) {
            e.printStackTrace();
        }
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment: filename=\"" + resource.getFilename() + "\"")
                .header(HttpHeaders.CONTENT_TYPE, "application/pdf")
                .body(resource);
    }

    @GetMapping(value = "/uploads/{filename:.+}")
    public ResponseEntity<Resource> verFoto(@PathVariable String filename) {
        Resource resource = null;
        try {
            resource = uploadFileService.load(filename);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment: filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }

    @RequestMapping(value = "/form", method = RequestMethod.POST)
    public String guardar(@Valid Cliente cliente, BindingResult result, Model model, @RequestParam("file") MultipartFile photo, RedirectAttributes flash, SessionStatus status) {
        if (result.hasErrors()) {
            model.addAttribute("titulo", "Formulario de cliente");
            return "form";
        }
        if (!photo.isEmpty()) {

            if (cliente.getId() != null && cliente.getId() > 0 && cliente.getPhoto() != null && cliente.getPhoto().length() > 0) {
                uploadFileService.delete(cliente.getPhoto());
            }
            String uniqueFilename;
            try {
                uniqueFilename = uploadFileService.copy(photo);
                flash.addFlashAttribute("info", "Ha subido correctamente '" + uniqueFilename + "'");
                cliente.setPhoto(uniqueFilename);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        String mensaje = (cliente.getId() == null) ? "Cliente creado con exito" : "Cliente editado con exito";
        clienteService.save(cliente);
        status.setComplete();
        flash.addFlashAttribute("success", mensaje);
        return "redirect:listar";
    }

    @RequestMapping(value = "/eliminar/{id}")
    public String eliminar(@PathVariable(value = "id") Long id, RedirectAttributes flash) {
        if (id > 0) {
            Optional<Cliente> clienteOptional = clienteService.findOne(id);
            Cliente cliente;
            if (clienteOptional.isPresent()) cliente = clienteOptional.get();
            else {
                flash.addFlashAttribute("error", "El ID del cliente no existe");
                return "redirect:/listar";
            }

            if (uploadFileService.delete(cliente.getPhoto()))
                flash.addFlashAttribute("info", "Foto " + cliente.getPhoto() + " eliminado con exito");
            clienteService.delete(id);
            flash.addFlashAttribute("success", "Cliente eliminado con exito");
        }
        return "redirect:/listar";
    }
}
