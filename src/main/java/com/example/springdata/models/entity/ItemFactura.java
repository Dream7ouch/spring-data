package com.example.springdata.models.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "prueba_items_factura", schema = "cas")
public class ItemFactura implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PRUEBAITEMFACTURASEQ")
    @SequenceGenerator(sequenceName = "prueba_item_factura_seq", allocationSize = 1, name = "PRUEBAITEMFACTURASEQ")
    @Column(length = 19)
    private Long id;

    private Integer quantity;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "producto_id")
    private Producto producto;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public Double calcularImporte() {
        return quantity.doubleValue() * producto.getPrice();
    }
}
