package com.example.springdata.models.service;

import net.sf.jasperreports.engine.JRException;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.MalformedURLException;
import java.sql.SQLException;
import java.util.Map;

public interface IUploadFileService {

    public Resource load(String filename) throws MalformedURLException;

    public Resource loadPdf(String filename, Map<String, Object> params) throws MalformedURLException, SQLException, JRException;

    public String copy(MultipartFile file) throws IOException;

    public boolean delete(String filename);

    public void deleteAll();

    public void init() throws IOException;

}
